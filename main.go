package main

import (
	"./nex_client"
	"fmt"
	"os"
)

func main() {
	c := nex_client.DefaultClient

	if len(os.Args) < 1 {
		fmt.Printf("Error: Please pass in the address.\n")
		return
	}

	//address := "nex://auragem.letz.dev/"
	address := os.Args[1]
	conn, err := c.Request(address) // TODO: Make sure this strips all '\r'
	if err != nil {
		fmt.Printf("Request failed: %s\n", err.Error())
		return
	}

	defer conn.Close()
	data, err2 := ReadAll(conn)
	if err2 != nil {
		fmt.Printf("ReadAll error: %s\n", err2)
		return
	}
	fmt.Printf("%s\n", data)
}
